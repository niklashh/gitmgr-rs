// TODO: Remove the allow at some point!
#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]

use git2::Repository;
use serde::de::{value, Visitor};
use std::env;
use std::io;
use std::path::{Path, PathBuf};
use url::Url;

mod arg_handler;
mod database;
mod list;
mod repo;
mod scan;
mod ssh;

use crate::database::Database;
use crate::repo::{Repo, RepoVisitor};

const DATABASE_PATH: &str = "./.gitmgr/config.toml";

fn main() -> io::Result<()> {
    let mut database_struct = Database::read(PathBuf::from(DATABASE_PATH))
        .expect("Tried to read the database");

    let matches = arg_handler::clap_app().get_matches();

    match matches.subcommand() {
        ("clone", Some(sub_m)) => {
            let keys = sub_m.value_of("SSH KEY");
            let mut builder = ssh::get_builder(keys);
            let repo_location = sub_m.value_of("REPOSITORY").unwrap();
            let repo_path = PathBuf::from(
                extract_name(&repo_location)
                    .expect("Tried to get the name of the repository."),
            );
            let repo = builder
                .clone(repo_location, &repo_path)
                .expect("Tried to clone repo");
            database_struct.add_repo(Repo(repo))?;
        }
        ("init", Some(sub_m)) => {
            let repo_path = sub_m.value_of("REPOSITORY").unwrap().to_string();
            let repo =
                Repository::init(&repo_path).expect("Tried to init repo");
            database_struct.add_repo(Repo(repo))?;
        }
        ("list", _) => {
            list::list(database_struct.get_repos());
        }
        ("scan", Some(sub_m)) => {
            let path =
                PathBuf::from(sub_m.value_of("DIRECTORY").unwrap().to_string());
            let repos = scan::scan(path);
            let blacklisted_dirs = database_struct.get_blacklisted_dirs();
            let repos = scan::filter_blacklisted(repos, blacklisted_dirs);
            let repos = database::filter_added_repos(
                repos,
                database_struct.get_repos(),
            );

            if sub_m.is_present("INTERACTIVE") {
                let repos = scan::repos_prompt(repos);
                for repo in repos {
                    database_struct.add_repo(repo)?;
                }
            } else {
                for repo in repos {
                    database_struct.add_repo(repo)?;
                }
            }
        }
        ("add", Some(sub_m)) => {
            let repo_path = sub_m.value_of("REPOSITORY").unwrap();
            let repo = RepoVisitor.visit_str::<value::Error>(repo_path)
                .expect("Tried to open repo.");
            database_struct.add_repo(repo)?;
        }
        ("rm", Some(sub_m)) => {
            let repo_path = sub_m.value_of("REPOSITORY").unwrap();
            let repo = RepoVisitor.visit_str::<value::Error>(repo_path)
                .expect("Tried to open repo.");
            database_struct.remove_repo(repo)?;
        }
        _ => println!("{}", matches.usage()),
    }

    database_struct
        .write(PathBuf::from(DATABASE_PATH))
        .expect("Tried to write to database");

    Ok(())
}

fn extract_name(location: &str) -> Result<std::ffi::OsString, String> {
    // TODO: Refactor!
    // First, see if `location` can be parsed as an url.
    match Url::parse(location) {
        // If parses, return file stem, else complain.
        Ok(url) => match PathBuf::from(url.path()).file_stem() {
            Some(stem) => Ok(stem.to_owned()),
            None => Err(format!("No repository at url {:?}", location)),
        },

        // If not parses, assume trying to clone using ssh,
        // and then try to get file stem.
        Err(_) => match location.split(":").nth(1) {
            Some(path) => match PathBuf::from(path).file_stem() {
                Some(stem) => Ok(stem.to_owned()),
                None => Err(format!("No repository at link {:?}", location)),
            },
            None => Err(format!("No repository at link {:?}", location)),
        },
    }
}
