use crate::database::Repos;
use crate::repo::Repo;
use colored::*;
use git2::StatusOptions;
use std::path::PathBuf;

pub fn format_repo(repo: &Repo) -> Result<String, git2::Error> {
    let is_bare = repo.is_bare();

    let name = if is_bare {
        repo.path().file_name().unwrap()
    } else {
        repo.path().parent().unwrap().file_stem().unwrap()
    };

    let mut revwalk = repo.revwalk()?;
    revwalk.push_head()?;
    let commits = revwalk.count();

    let mut options = StatusOptions::new();
    options.include_ignored(false);
    let statuses = repo.statuses(Some(&mut options))?;
    let num_unstaged = statuses
        .iter()
        .filter(|entry| !entry.status().contains(git2::Status::IGNORED))
        .count();

    Ok(format!(
        "{} {} {} {}",
        is_bare,
        name.to_str().unwrap().blue(),
        commits,
        num_unstaged.to_string().red()
    ))
}

pub fn list(repos: &Repos) {
    for repo in repos {
        match format_repo(repo) {
            Ok(data) => println!("{}", data),
            Err(error) => println!("Unable to format: {:?}", error),
        }
    }
}
