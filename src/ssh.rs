
use git2::{Cred, FetchOptions, RemoteCallbacks, Repository};
use git2::build::RepoBuilder;
use std::path::{Path, PathBuf};
use thrussh_config::parse_home;
use std::env;

pub fn get_builder(keys: Option<&str>) -> RepoBuilder {
    let mut callbacks = RemoteCallbacks::new();
    // TODO: Add the possibility of using ssh agent
    // TODO: Refactor this horror code.
    //       We tried to move the match inside the closure,
    //       but that resulted to all kinds of odd ownership error.
    match keys {
        Some(path) => callbacks.credentials(move |_, username, _|
                                            Cred::ssh_key(
                                                username.unwrap(),
                                                None,
                                                Path::new(&path),
                                                None
                                            )
        ),
        None => callbacks.credentials(|url, username, _|
                                     Cred::ssh_key(
                                        username.unwrap(),
                                        None,
                                        &key_path(url),
                                        None
                                     )

        ),
    };
    let mut fetch_opt = FetchOptions::new();
    fetch_opt.remote_callbacks(callbacks);
    let mut builder = RepoBuilder::new();
    builder.fetch_options(fetch_opt);
    return builder;
}

fn key_path(url: &str) -> PathBuf {
    // Get the path to the ssh keys

    // Get the hostname by splitting the url
    // i.e. "git@domain.com:repo" -> "domain.com"
    let host = url.split("@").nth(1).unwrap().split(":").next().unwrap();
    parse_home(host).ok()
                    .map(|conf| conf.identity_file)
                    .flatten()
                    .map(|file| PathBuf::from(file))
                    .unwrap_or_else(|| {
                        vec![format!("{}/.ssh/id_rsa", env::var("HOME").unwrap()),
                             format!("{}/.ssh/id_ed25519", env::var("HOME").unwrap())]
                            .iter()
                            .map(|path| get_file(path))
                            .filter(|file| file.is_some())
                            .next().unwrap_or_else(|| panic!("No private keys found in {}/.ssh. Consider using the -i flag.", env::var("HOME").unwrap()))
                                    .unwrap()
                    })
}

// TODO: Maybe come up with a smarter way of doing this.
fn get_file(path: &str) -> Option<PathBuf> {
    let pathbuf = PathBuf::from(path);
    if pathbuf.is_file() {
        Some(pathbuf)
    } else {
        None
    }
}
