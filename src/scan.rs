use crate::database::{BlacklistedDirs, Repos};
use crate::repo::Repo;
use git2::Repository;
use rustyline::error::ReadlineError;
use rustyline::Editor;
use std::fs::canonicalize;
use std::path::PathBuf;
use walkdir::WalkDir;

pub fn scan(dir: PathBuf) -> Repos {
    WalkDir::new(dir)
        .into_iter()
        .filter_map(|entry| {
            let entry = entry.ok()?;
            if entry.file_type().is_dir() {
                let dir_name = entry.file_name().to_string_lossy();
                if dir_name == ".git" {
                    let path = entry.into_path();
                    let repo = Repository::open(&path).expect(
                        &format!("Could not open repository {:?}", path)
                            .as_str(),
                    );
                    return Some(Repo(repo));
                }
            }
            None
        })
        .collect()
}

pub fn filter_blacklisted(scan: Repos, blacklist: &BlacklistedDirs) -> Repos {
    scan.into_iter()
        .filter(|repo| {
            !blacklist.iter().any(|b| {
                let b = canonicalize(b)
                    .expect(
                        format!(
                            "failed to canonicalize blacklisted dir {:?}",
                            b
                        )
                        .as_str(),
                    )
                    .to_string_lossy()
                    .into_owned();
                repo.path().ancestors().any(|r| {
                    let r = r.to_string_lossy().into_owned();
                    b == r
                })
            })
        })
        .collect()
}

pub fn repos_prompt(repos: Repos) -> Repos {
    let mut rl = Editor::<()>::new();
    repos
        .into_iter()
        .filter_map(|repo| {
            let prompt = format!("Add {:?}? [Y/n] ", repo);
            let readline = rl.readline(prompt.as_str());
            match readline {
                Ok(line) => {
                    let line = line.to_lowercase();
                    if line == "y" || line == "" {
                        return Some(repo);
                    }
                }
                Err(ReadlineError::Interrupted) => {
                    panic!("CTRL-C");
                }
                Err(ReadlineError::Eof) => {
                    panic!("CTRL-D");
                }
                Err(err) => {
                    panic!("Error: {:?}", err);
                }
            };
            None
        })
        .collect()
}
