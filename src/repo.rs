use git2::Repository;
use serde::de::{self, Visitor};
use serde::ser;
use serde::{Deserialize, Serialize, Serializer};
use std::fmt;
use std::fs::canonicalize;
use std::hash::{Hash, Hasher};
use std::marker::PhantomData;
use std::path::PathBuf;

pub struct Repo(pub Repository);

impl Hash for Repo {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.path().hash(state)
    }
}

impl std::ops::Deref for Repo {
    type Target = Repository;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl PartialEq for Repo {
    fn eq(&self, other: &Self) -> bool {
        self.path() == other.path()
    }
}
impl Eq for Repo {}

impl fmt::Debug for Repo {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(&self.path().to_string_lossy().to_owned())
    }
}

impl Serialize for Repo {
    fn serialize<S>(
        &self,
        serializer: S,
    ) -> std::result::Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.path().to_string_lossy().to_owned())
    }
}

pub struct RepoVisitor;

impl<'de> Visitor<'de> for RepoVisitor {
    type Value = Repo;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("str which is a git repository path expected")
    }

    fn visit_str<E>(
        self,
        repo_path_str: &str,
    ) -> std::result::Result<Self::Value, E>
    where
        E: de::Error,
    {
        match Repository::open(repo_path_str) {
            Ok(repository) => Ok(Repo(repository)),
            Err(error) => Err(E::custom(error)),
        }
    }
}

impl<'de> Deserialize<'de> for Repo {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_str(RepoVisitor)
    }
}
