use crate::repo::Repo;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use std::fs::{canonicalize, create_dir_all, File, OpenOptions};
use std::io::{Read, Result, Write};
use std::path::PathBuf;

const DEFAULT_BLACKLIST: [&str; 3] = ["~/.config", "~/.cache", "~/.cargo"];

pub type BlacklistedDirs = HashSet<PathBuf>;
pub type Repos = HashSet<Repo>;

#[derive(Debug, Serialize, Deserialize)]
pub struct Database {
    blacklisted_dirs: BlacklistedDirs,
    repos: Repos,
}

impl Database {
    /// Returns the database.
    /// If no database was found,
    /// return empty database with a default blacklist.
    pub fn read(path: PathBuf) -> Result<Database> {
        let database: Database = if path.exists() {
            let file: Vec<u8> = File::open(path)?
                .bytes()
                .map(|byte| byte.unwrap())
                .collect();
            let deser_base: Database = toml::from_slice(file.as_slice())?;
            Database {
                blacklisted_dirs: handle_tildes(deser_base.blacklisted_dirs),
                repos: deser_base.repos,
            }
        } else {
            let dirs_vector = DEFAULT_BLACKLIST
                .iter()
                .map(|path| PathBuf::from(path))
                .collect();
            Database {
                blacklisted_dirs: handle_tildes(dirs_vector),
                repos: HashSet::new(),
            }
        };

        Ok(database)
    }

    pub fn write(&self, path: PathBuf) -> Result<()> {
        create_dir_all(path.parent().unwrap())?;
        let mut file = OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .truncate(true)
            .open(path)?;
        file.write_all(toml::to_string_pretty(&self).unwrap().as_bytes())?;
        Ok(())
    }

    pub fn get_repos(&mut self) -> &Repos {
        &self.repos
    }

    pub fn get_blacklisted_dirs(&mut self) -> &BlacklistedDirs {
        &self.blacklisted_dirs
    }

    /// Push a new repo to the `repos` vector.
    pub fn add_repo(&mut self, repo: Repo) -> Result<&Database> {
        self.repos.insert(repo);
        Ok(self)
    }

    /// Push a new dir to the `blacklisted_dir` vector.
    pub fn add_to_blacklist(&mut self, dir: PathBuf) -> Result<&Database> {
        self.blacklisted_dirs.insert(canonicalize(dir)?);
        Ok(self)
    }

    /// Retain only the paths,
    /// that aren't equal to the given path.
    pub fn remove_repo(&mut self, repo: Repo) -> Result<&Database> {
        self.repos.retain(|x| *x != repo);
        Ok(self)
    }

    /// Retain only the paths,
    /// that aren't equal to the given path.
    pub fn remove_from_blacklist(&mut self, dir: PathBuf) -> Result<&Database> {
        let handled_dir = canonicalize(dir)?;
        self.blacklisted_dirs.retain(|x| *x != handled_dir);
        Ok(self)
    }
}

fn handle_tildes(paths: HashSet<PathBuf>) -> HashSet<PathBuf> {
    paths
        .into_iter()
        .map(|path| match path.strip_prefix("~") {
            Ok(rest) => {
                let home_dir = std::env::var("HOME").unwrap();
                PathBuf::from(home_dir).join(rest)
            }
            // Doesn't start with ~, so we return original path.
            Err(_) => path,
        })
        .collect()
}

pub fn filter_added_repos(mut repos: Repos, added_repos: &Repos) -> Repos {
    repos.retain(|repo| !added_repos.contains(repo));
    repos
}
